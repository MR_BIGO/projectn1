package com.example.a1stassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val randomNumberGenerator = findViewById<Button>(R.id.randomNumberGenerator)
        val randomNumberTextView = findViewById<TextView>(R.id.randomNumberTextView)
        randomNumberGenerator.setOnClickListener {
            val number: Int = randomNumber()
            d("randomNumber", "This number can be divided by five ${divisionByFive(number)}")
            randomNumberTextView.text = number.toString()
        }
    }

    private fun randomNumber() = (-100..100).random()


    private fun divisionByFive(randomNumber:Int):Boolean{
        return (randomNumber % 5 == 0)
    }
}


